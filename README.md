<h1>TE-LINE 标签显示</h1>

> 是一个基于Vu2.x的标题显示组件，支持4种不同颜色尺寸以及样式

| 属性        | 说明   |  描述  |
| --------   | -----:  | :----:  |
| title     | 显示的标题 | 非必填     |
| size        | 尺寸大小   | 非必填：default（默认，可不设定），small，big |
| type        | 颜色样式    |  非必填：primary（默认可不设定），success，warning，error  |

| 插槽        | 说明   |  描述  |
| --------   | -----:  | :----:  |
| descript     | 副标题：对标题的补充 | 非必填：默认居左，align="right"，会居右显示     |

#### 效果预览
<img src="https://github.com/qiaen/static/blob/master/12082.png?raw=true" alt="">

#### 开始使用
```javascript
// main.js
import Vue from 'vue'
import TeLine from 'te-line'

Vue.use(TeLine)
```
#### 在.vue文件中使用`te-line`标签
```html
<te-line title="最小化" type="success" size="small">
	<span slot="descript" align="right">附属提示，居右显示</span>
</te-line>

<te-line title="默认" type="primary" size="small">
	<span slot="descript">你可以从下属列表中选择一个类型</span>
</te-line>

<te-line title="突出显示-警告" size="small" type="warning">
	<span slot="descript">为了突出重要性，加粗显示</span>
</te-line>

<te-line title="突出显示-错误" size="small" type="error">
	<span slot="descript">为了突出重要性，加粗显示</span>
</te-line>
```
##### 花木兰 基于 `Vue3` `TS` `Vite`的前端框架
在线预览[http://mulan.diumx.com](http://mulan.diumx.com "http://mulan.diumx.com")
##### 兰陵王 基于 `Vue2.x` `Webpack`的前端框架
在线预览[https://lanling.diumx.com](http://lanling.diumx.com "http://lanling.diumx.com")

