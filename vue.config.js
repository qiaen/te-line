const path = require('path')
let entry = "./test/main.js";
let filename = "build.js";

if (process.env.NODE_ENV !== "development") {
	entry = "./src/index.js";
	filename = "te-line.min.js";
}
const base = {
	isProd: process.env.NODE_ENV === 'production',
	// ！！重要！！发布后的项目地址
	href: '',
	// 项目页面title
	name: '兰陵王 - 后台管理系统',
	// 目标接口域名
	target: 'https://lanling.diumx.com',
	// target: 'http://localhost:8030',
}
module.exports = {
	pages: {
		index: {
			// page 的入口
			entry: 'test/main.js',
			// 在 dist/index.html 的输出
			filename: 'index.html'
		}
	},
	devServer: {
		host: process.env.HOST,
		port: process.env.PORT && Number(process.env.PORT),
		open: false,
		overlay: {
			warnings: false,
			errors: true
		}
	},
	configureWebpack: {},
	css: {
		// 组件的样式是否另外打包成单独的css文件。默认为true
		extract: false
	},
	productionSourceMap: false
}